Locust TileKit
==============

A (for now) little kit to easily create Tiles for video games and more.    

![](http://i.imgur.com/dqiM5fH.png)    

### [→ Download latest Version](https://bitbucket.org/rockfort/modo-locust-tilemaker/get/master.zip)

* * *

## Bugs

  + PopOut Layout doesn't work yet

## Todo

  + Allow multiple objects to be moved
  + Move in the axis of the viewport
  + Alternative commands:
      + Move in Y Axis
      + Rotate ± 90deg