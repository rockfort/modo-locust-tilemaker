#python
##============================================================================##
# Move Item by the Boundaries
##----------------------------------------------------------------------------##
#
# Description:
# Moves an item by the boundaries in the given direction
#
# Useage:
# @aw_move_item direction
# 
# Possible directions: 
# topleft, top, topright, left, center, right, bottomleft, bottom, bottomright
# 
##============================================================================##

#==============================================================================#
# Imports                                                                       
#==============================================================================#

import sys
from lx import eval as lxe, evalN as lxN, eval1 as lx1, out as lxo
import traceback

################################################################################
# Functions
################################################################################

# [move_item]
# Moves an item by the boundaries in the given direction
# move_item("direction")
def move_item(direction):

  # Get the bounds of the current item
  bounds = lx.eval("query layerservice layer.bounds ? current")

  # Assign the bounds in each axis
  # Has to be converted to a float to be useable
  bound_x = float(format((bounds[3]-bounds[0]),'.3f'))
  # bound_y = format((bounds[4]-bounds[1]),'.3f')
  bound_z = float(format((bounds[5]-bounds[2]),'.3f'))

  pos_x = lxe('transform.channel pos.X ?')
  # pos_y = lxe('transform.channel pos.Y ?')
  pos_z = lxe('transform.channel pos.Z ?')

  # Default values for the x,z
  x = pos_x
  z = pos_z

  # Check for the input directions

  # topLeft | top | topRight
  if direction == "upleft":
    x = x - bound_x
    z = z - bound_z
  elif direction == "up":
    z = z - bound_z
  elif direction == "upright":
    x = x + bound_x
    z = z - bound_z

  # left | center | right
  if direction == "left":
    x = x - bound_x
  elif direction == "center":
    z = 0
    x = 0
  elif direction == "right":
    x = x + bound_x

  # bottomLeft | bottom | bottomRight
  if direction == "downleft":
    x = x - bound_x
    z = z + bound_z
  elif direction == "down":
    z = z + bound_z
  elif direction == "downright":
    x = x + bound_x
    z = z + bound_z

  # Move the item
  lxe('transform.channel pos.X %s' % x)
  lxe('transform.channel pos.Z %s' % z)

################################################################################
# Main
################################################################################

def main():

  # Get the input parameters
  args = lx.args()

  # For every passed argument
  # Move the item in the direction of the passed argument
  for arg in args:
    move_item(arg)


# Makes line numbers more readable
if __name__ == '__main__':
   try:
      main()
   except:
      lx.out(traceback.format_exc())